# Herramientas
A continuación compartire las herramientas que mas utilizo en mi día a día, con una pequeña descripción de su utilidad y funcionamiento.
- [Herramientas](#herramientas)
  - [## Editores de Texto](#-editores-de-texto)
    - [Visual Studio Code](#visual-studio-code)
    - [Sublime Text](#sublime-text)
---
## Editores de Texto
---
### Visual Studio Code
Visual Studio Code es un potente editor de texto open source desarrollada por Microsoft. Este permite la instalación de cientos de librerias las cuales sirven para agilizar los procesos del día a día, o bien incorporar varios entornos de desarrollo en un solo lugar, y con un software bastante liviano y agil. Entre las principales herramientes recomendadas estan las siguientes:
- #### Markdown All in One
  - Esta permite la previsualización de archivos markdown, así como una lista de atajos y highlights de sintaxis para este "lenguaje".
- #### Prettier
  - a
- #### Remote - SSH
  - a
- #### Material Icon Theme
  - Conjunto de iconos basados en Material Design.
- #### JavaScript (ES6) code snippets
  - a
- #### Docker
  - a

---
### Sublime Text
Visual Studio Code es un potente editor de texto open source desarrollada por Microsoft. Este permite l